'use strict';

/* ------------------------------------------------------------------------
| 	IndexController.JS
---------------------------------------------------------------------------
|
|	Index controller.
|
------------------------------------------------------------------------- */

module.exports = function (args) {

	var q = args.MODULES.Q;

	var IndexController = {

		/******************************************************************
		|	Author: @ais
		|	Description: Sample middleware
		******************************************************************/

		getIndex: function (previousData, req, res, next) {
			return res.status(200).json({ code: 200, status: 'success', data: { name: 'project-boilerplate' }});
		}
	};

	return IndexController;
};
