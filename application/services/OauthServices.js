'use strict';

/* ------------------------------------------------------------------------
| 	UserServices.JS
---------------------------------------------------------------------------
|
|	User services.
|
|	Author: @ais
|	Latest updates:
|	20 Oct 2015 - @ais - Create User services.
|
------------------------------------------------------------------------- */

module.exports = function (args) {

	let crypto	= args.MODULES.CRYPTO;
	let helpers = args.helpers;

	/******************************************************************
	|	Author: @ais
	|	Description: Init model/schema.
	******************************************************************/

	var UsersModel 		= args.models.Users;
	var AhsModel 		= args.models.Ahs;
	var ConsumersModel 	= args.models.Consumers;
	var AccessTokenModel = args.models.AccessToken;

	var OauthServices = {

		/******************************************************************
		|	Author: @ais
		|	Description: Check clientId & clientSecret
		******************************************************************/
		
		getClient: function (clientId, clientSecret, cb) {
			if (clientId && clientSecret) 
				return cb(null, { clientId: clientId, clientSecret: clientSecret });

			cb();
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Check grantType
		******************************************************************/
		
		grantTypeAllowed: function (clientId, grantType, cb) {
			if (clientId && grantType == 'password')
				return cb(false, true);
			
			cb();
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Get user data
		******************************************************************/
		
		getUser: function (username, password, cb) {
			password = helpers.Common.convertPassword(crypto, password);

			let opts = {
				user: {
					user_name: username,
					password: password,
					is_active: true
				},
				ahs: {
					user_name: username,
					user_password: password
				}
			};

			UsersModel.find({ where: opts.user }).nodeify(function (err, user){
				if (err) return cb(err);
				if (user && user.dataValues) return cb(null, { user: user.dataValues });
				
				if (!user) {
					AhsModel.find({ where: opts.ahs }).nodeify(function (err, ahs){
						if (err) return cb(err);
						if (!ahs) return cb({ code: 401, status: 'error', message: "Credentials can't be found." });
						if (ahs && ahs.dataValues) return cb(null, { ahs: ahs.dataValues });

						cb({ code: 401, status: 'error', message: "Credentials can't be found." });
					});
				} else {
					cb({ code: 401, status: 'error', message: "Credentials can't be found." });
				}
			});
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Save generated access token
		******************************************************************/
		
		saveAccessToken: function (accessToken, clientId, expires, objData, deviceToken, cb) {
			let base_id = (objData.user) ? objData.user.id : objData.ahs.id ;
			let access_role = (objData.user) ? 'command-center' : 'ahs' ;

			let searchParams = {
				base_id: base_id, 
				access_role: access_role
			};

			AccessTokenModel.findOne({ where: searchParams }).nodeify(function (err, data) {
				if (err) return cb(err);

				if (!data) {
					let params = {
						access_token: accessToken,
						access_token_expire: expires,
						device_token: (!deviceToken || deviceToken == 'n/a') ? false : deviceToken,
						base_id: base_id,
						access_role: (objData.user) ? 'command-center' : 'ahs'
					};

					AccessTokenModel.create(params).nodeify(cb);
				} else {
					let updateParams = {
						access_token: accessToken,
						access_token_expire: expires,
						device_token: (!deviceToken || deviceToken == 'n/a') ? false : deviceToken
					};

					var whereOpts = {
						base_id: base_id,
						access_role: access_role
					};

					AccessTokenModel.update(updateParams, { where: whereOpts }).nodeify(cb);					
				}
			});
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Get all record from `User`
		******************************************************************/
		
		authenticate: function (accessToken, cb) {
			AccessTokenModel.findOne({ where: { access_token: accessToken } }).nodeify(cb);
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Get all record from `User`
		******************************************************************/
		
		authenticateConsumer: function (accessToken, cb) {
			ConsumersModel.findOne({ where: { access_token: accessToken } }).nodeify(cb);
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Save new access token
		******************************************************************/

		saveNewAccessToken: function (params, cb) {
			let accessTokenParams = {
				access_token: params.accessToken,
				access_token_expire: params.expires,
				device_token: (params.deviceToken) ? params.deviceToken : false,
				base_id: params.base_id,
				access_role: params.access_role
			};

			let whereOpts = {
				base_id: params.base_id,
				access_role: params.access_role
			};

			AccessTokenModel.findOne({ where: whereOpts }).nodeify(function (err, dataAccessToken) {
				if (err) return cb(err);

				if (dataAccessToken) {
					let updateParams = {
						access_token: params.accessToken,
						access_token_expire: params.expires,
						device_token: (!params.deviceToken || params.deviceToken == 'n/a') ? false : params.deviceToken
					};

					AccessTokenModel.update(updateParams, { where: whereOpts }).nodeify(function (err, token) {
						if (err) return cb(err);
						return cb(null, accessTokenParams);
					});
				} else {
					AccessTokenModel.create(accessTokenParams).nodeify(cb);
				}
			});
		}

	}

	return OauthServices;
};