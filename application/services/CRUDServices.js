'use strict';

/* ------------------------------------------------------------------------
| 	CRUDServices.JS
---------------------------------------------------------------------------
|
|	CRUD services.
|
|	Author: @ais
|	Latest updates:
|	20 Oct 2015 - @ais - Create CRUD services.
|
------------------------------------------------------------------------- */

module.exports = function (args) {

	/******************************************************************
	|	Author: @ais
	|	Description: Init model/schema.
	******************************************************************/

	var models = args.models;

	var CRUDServices = {

		/******************************************************************
		|	Author: @ais
		|	Description: Get all record from `:modelName`
		******************************************************************/
		
		findAll: function (modelName, cb) {
			models[modelName].findAll().nodeify(cb);
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Get a record from `:modelName` with `:opts`
		******************************************************************/
		
		findOne: function (modelName, opts, cb) {
			models[modelName].findOne({ where: opts }).nodeify(cb);
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Get a record from `:modelName` with `:id`
		******************************************************************/
		
		findById: function (modelName, id, cb) {
			models[modelName].findById(id).nodeify(cb);
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Get a record from `:modelName` with `:opts`
		******************************************************************/
		
		create: function (modelName, opts, cb) {
			models[modelName].create(opts).nodeify(cb);
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Update a record from `:modelName` with `:id` and `:opts`
		******************************************************************/
		
		update: function (modelName, id, opts, cb) {
			var whereOpts = { where: { id: id } };
			models[modelName].update(opts, whereOpts).nodeify(cb);
		},

		/******************************************************************
		|	Author: @ais
		|	Description: delete a record from `:modelName` with `:opts`
		******************************************************************/
		
		destroy: function (modelName, opts, cb) {
			models[modelName].destroy({ where: opts }).nodeify(cb);
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Get all record from `:modelName` with `:opts`
		******************************************************************/
		
		findAllWithOpts: function (modelName, opts, cb) {
			models[modelName].findAll({ where: opts }).nodeify(cb);
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Get all record from `:modelName` with custom `:opts`
		******************************************************************/
		
		findAllWithCustomOpts: function (modelName, opts, cb) {
			models[modelName].findAll(opts).nodeify(cb);
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Using `Sum` from `:modelName` with `:opts`
		******************************************************************/
		
		sum: function (modelName, field, opts, cb) {
			models[modelName].sum(field, { where: opts }).nodeify(cb);
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Using `Sum` from `:modelName` with `:opts`
		******************************************************************/
		
		count: function (modelName, opts, cb) {
			models[modelName].count({ where: opts }).nodeify(cb);
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Using `Sum` from `:modelName` with `:opts`
		******************************************************************/

		insertBulk: function (modelName, opts, cb) {
			models[modelName].bulkCreate(opts).nodeify(cb);
		}
	}

	return CRUDServices;
};