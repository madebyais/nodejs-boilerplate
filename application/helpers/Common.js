/* ------------------------------------------------------------------------
| 	Common.js
---------------------------------------------------------------------------
|
|	A common helper that often used.
|
|	Author: @ais
|	Latest updates:
|	20 Oct 2015 - @ais - Create a common helpers.
|
------------------------------------------------------------------------- */

'use strict';

var Common = {
	
	convertPassword: function (crypto, password) {
		return crypto.createHash("sha256").update(password).digest("hex");
	}

};

module.exports = Common;