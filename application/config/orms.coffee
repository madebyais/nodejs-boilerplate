module.exports =

	common:

		### --------------------------------------
		|	Set to `true`
		|	If you want to enable
		|	the relations/associations
		|-------------------------------------- ###

		enableRelations: false

		### --------------------------------------
		|	Relations/associations configuration
		|
		|	Example:
		|
		|	{
		|		parentTable: 'Player',	--> Source table. It's based on model file name.
		|		type: 'belongsTo',		--> Please refer to http://docs.sequelizejs.com/en/latest/docs/associations
		|		childTable: 'Team',		--> Target table. It's based on model file name.
		|		opts: { as: 'Role' } 	--> if any, if not then you can leave it as {}
		|	}
		|
		|	Generated code:
		|
		|	Player.belongsTo(Team, { as: 'Role' });
		|
		|-------------------------------------- ###


		relations: []


	development: {}

	staging: {}

	production: {}
