module.exports =

	_excludePath:	[ 'controllers', 'helpers', 'models', 'queues', 'routers', 'services' ]

	common:			__dirname + '/common'
	connections: 	__dirname + '/connections'
	orms: 			__dirname + '/orms'
	credentials:	__dirname + '/credentials'
	error:			__dirname + '/error'

	controllers:	__dirname + '/../controllers/'
	helpers:		__dirname + '/../helpers/'
	models:			__dirname + '/../models/'
	queues: 		__dirname + '/../queues/'
	routers:		__dirname + '/../routers/'
	services:		__dirname + '/../services/'
	