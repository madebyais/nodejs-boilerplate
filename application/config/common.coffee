module.exports =

	common:
		APPLICATION_HOST: '0.0.0.0'
		APPLICATION_PORT: 9000

		LOG:
			default: __dirname + '/../logs/logs.log'
			exceptions: __dirname + '/../logs/exceptions.log'

		LOCALIZATION: {}

	development: {}

	staging: {}

	production: {}
