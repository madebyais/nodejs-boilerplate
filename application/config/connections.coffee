module.exports =

	common: {

		syncForce: false

		databases: [
			name: 'postgresql'
			type: 'postgres'
			host: '0.0.0.0'
			port: 5432
			dbName: 'db_dev'
			username: 'postgres'
			password: ''
		]

		redis: 
			host: 'localhost'
			port: 6379
			database: 
				queue: 1

	}

	development: {}

	staging: {}
	
	production: {}