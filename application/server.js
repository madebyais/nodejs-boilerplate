'use strict';

/* ------------------------------------------------------------------------
| 	APP.JS
---------------------------------------------------------------------------
|
|	Configure application base.
|
|	Author: @ais
|	Latest updates:
|	N/A
|
------------------------------------------------------------------------- */

module.exports = function (MODULES, CONFIG) {

	/* ------------------------------------------------------------------------
	| 	INITIALIZE LIBRARY
	---------------------------------------------------------------------------
	|
	|	Initialize library
	|
	|	Author: @ais
	|	Latest updates:
	|	21 Oct 2015 - @kuncara - Initialize library
	|
	------------------------------------------------------------------------- */

	console.time('requireLib')

	let redisClient = MODULES.REDIS.createClient({
		host: CONFIG.connections.redis.host,
		port: CONFIG.connections.redis.port
	});

	MODULES.REDIS = redisClient;

	MODULES.Q = MODULES.KUE.createQueue({
		prefix: 'q',
		redis: {
			host: CONFIG.connections.redis.host,
			port: CONFIG.connections.redis.port,
			db: CONFIG.connections.redis.database.queue
		}
	});

	console.timeEnd('requireLib')


	/* ------------------------------------------------------------------------
	| 	CONFIG LOG FILE
	---------------------------------------------------------------------------
	|
	|	Configure application logs using WinstonJS.
	|
	|	Author: @ais
	|	Latest updates:
	|	13 Oct 2015 - @ais - Create configuration for log
	|
	------------------------------------------------------------------------- */

	console.time('InitializeLogger');
	var logOpts = {
		transports: [
			new (MODULES.WINSTON.transports.Console)({ colorize: true }),
			new (MODULES.WINSTON.transports.File)({ filename: CONFIG.common.LOG.default, handleExceptions: true, colorize: true })
		],
		exceptionHandlers: [
			new (MODULES.WINSTON.transports.Console)({ colorize: true }),
			new (MODULES.WINSTON.transports.File)({ filename: CONFIG.common.LOG.exceptions, handleExceptions: true, colorize: true })
		]
	};
	var log = new (MODULES.WINSTON.Logger)(logOpts);
	MODULES.LOG = log;
	console.timeEnd('InitializeLogger');

	/* ------------------------------------------------------------------------
	| 	APPLICATION ENGINE
	---------------------------------------------------------------------------
	|
	|	Configure & initialize application using ExpressJS.
	|
	|	Author: @ais
	|	Latest updates:
	|	13 Oct 2015 - @ais - Create application engine
	|
	------------------------------------------------------------------------- */

	console.time('InitializeApp');
	var app = MODULES.EXPRESS();

	app.use (MODULES.BODY_PARSER.urlencoded({ extended: true }));
	app.use (MODULES.BODY_PARSER.json({ extended: true }));
	app.use (MODULES.CORS());
	app.use (MODULES.EXPRESS_LOGGER.create(log));
	app.use (MODULES.METHOD_OVERRIDE());
	app.use (MODULES.MULTER());
	app.disable ('x-powered-by');
	console.timeEnd('InitializeApp');

	var args = { MODULES: MODULES, CONFIG: CONFIG, app: app };

	/* ------------------------------------------------------------------------
	| 	APPLICATION HELPERS
	---------------------------------------------------------------------------
	|
	|	Initialize application helpers.
	|
	|	Author: @ais
	|	Latest updates:
	|	14 Oct 2015 - @ais - Create application helpers
	|
	------------------------------------------------------------------------- */

	args.helpers = require(__dirname + '/../system/helper')(args);

	/* ------------------------------------------------------------------------
	| 	APPLICATION MODELS
	---------------------------------------------------------------------------
	|
	|	Initialize application models.
	|
	|	Author: @ais
	|	Latest updates:
	|	14 Oct 2015 - @ais - Create application models
	|
	------------------------------------------------------------------------- */

	args.models = require(__dirname + '/../system/model')(args);

	/* ------------------------------------------------------------------------
	| 	APPLICATION SERVICES
	---------------------------------------------------------------------------
	|
	|	Initialize application services.
	|
	|	Author: @ais
	|	Latest updates:
	|	14 Oct 2015 - @ais - Create application services
	|
	------------------------------------------------------------------------- */

	args.services = require(__dirname + '/../system/service')(args);

	/* ------------------------------------------------------------------------
	| 	APPLICATION CONTROLLER
	---------------------------------------------------------------------------
	|
	|	Initialize application controllers.
	|
	|	Author: @ais
	|	Latest updates:
	|	14 Oct 2015 - @ais - Create application controller
	|
	------------------------------------------------------------------------- */

	args.controllers = require(__dirname + '/../system/controller')(args);

	/* ------------------------------------------------------------------------
	| 	OAUTH 2.0
	---------------------------------------------------------------------------
	|
	|	Initialize application oauth.
	|
	|	Author: @ais
	|	Latest updates:
	|	21 Oct 2015 - @ais - Create application oauth
	|
	------------------------------------------------------------------------- */

	app.oauth = MODULES.OAUTH2({
		model: args.services.OauthServices,
		grants: [ 'password' ],
		debug: true
	});

	/* ------------------------------------------------------------------------
	| 	APPLICATION ROUTER
	---------------------------------------------------------------------------
	|
	|	Initialize application router.
	|
	|	Author: @ais
	|	Latest updates:
	|	14 Oct 2015 - @ais - Create application router
	|
	------------------------------------------------------------------------- */

	require(__dirname + '/../system/router')(args);

	/* ------------------------------------------------------------------------
	| 	APPLICATION QUEUE
	---------------------------------------------------------------------------
	|
	|	Initialize application queue.
	|
	|	Author: @ais
	|	Latest updates:
	|	29 Oct 2015 - @ais - Create application queue
	|
	------------------------------------------------------------------------- */

	require(__dirname + '/../system/queue')(args);

	MODULES.KUE_UI.setup({
		apiURL: '/queue-api',
		baseURL: '/queue-dashboard',
		updateInterval: 5000
	});

	app.use('/queue-api', MODULES.KUE.app);
	app.use('/queue-dashboard', MODULES.KUE_UI.app);

	/* ------------------------------------------------------------------------
	| 	START APPLICATION ENGINE
	---------------------------------------------------------------------------
	|
	|	Start application engine using :APPLICATION_HOST & :APPLICATION_PORT
	|
	|	Author: @ais
	|	Latest updates:
	|	13 Oct 2015 - @ais - Create function to start application engine
	|
	------------------------------------------------------------------------- */

	console.time('StartApp');
	app.listen (CONFIG.common.APPLICATION_PORT, CONFIG.common.APPLICATION_HOST);
	console.timeEnd('StartApp');

	console.log("===================================");
	console.log("|          SERVER STARTED         |");
	console.log("|    HOST: " + CONFIG.common.APPLICATION_HOST + ", PORT: " + CONFIG.common.APPLICATION_PORT + "    |");
	console.log("===================================");
};