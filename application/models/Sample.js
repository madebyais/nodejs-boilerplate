'use strict';

/* ------------------------------------------------------------------------
| 	AccessToken.JS
---------------------------------------------------------------------------
|
|	Access token schema.
|
|	Author: @ais
|	Latest updates:
|	03 Nov 2015 - @ais - Create access token schema
|
------------------------------------------------------------------------- */

module.exports = function (Sequelize, DataTypes) {

	var Sample = Sequelize.define('sample', {
		username: {
			type: DataTypes.STRING,
			allowNull: false
		},
		password: {
			type: DataTypes.STRING,
			allowNull: false
		},
		fullname: {
			type: DataTypes.STRING,
			allowNull: false	
		}
	}, {
		paranoid: true,
		underscored: true
	});

	return Sample;
}
