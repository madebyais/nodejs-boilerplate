'use strict';

console.log("===================================");
console.log("|           APOLLO ENGINE         |");
console.log("===================================");

/* ------------------------------------------------------------------------
| 	REGISTER COFFEE-SCRIPT
------------------------------------------------------------------------- */

require('coffee-script/register');

/* ------------------------------------------------------------------------
| 	REGISTER NEWRELIC
---------------------------------------------------------------------------
|
|	Only loaded for non-development environment to avoid wrong metrics
|
------------------------------------------------------------------------- */

if (process.env.NODE_ENV !== 'development') {
	require('newrelic');
}

/* ------------------------------------------------------------------------
| 	IMPORT NODE MODULES & SYSTEM INIT
---------------------------------------------------------------------------
|
|	Import all node modules and system init.
|
|	Author: @ais
|	Latest updates:
|	13 Oct 2015 - @ais - Created modules variable
|
------------------------------------------------------------------------- */

var MODULES = {
	ASYNC: 				require('async'),
	BODY_PARSER: 		require('body-parser'),
	CORS: 				require('cors'),
	CRYPTO: 			require('crypto'),
	DEBUG: 				require('debug'),
	ERROR_HANDLER: 		require('errorhandler'),
	EXPRESS: 			require('express'),
	EXPRESS_LOGGER: 	require('express-request-logger'),
	FS: 				require('fs'),
	I18N: 				require('i18n'),
	KUE: 				require('kue'),
	KUE_UI: 			require('kue-ui'),
	METHOD_OVERRIDE: 	require('method-override'),
	MULTER: 			require('multer'),
	OAUTH2: 			require('ais-oauth2-server'),
	NEXMO:				require('simple-nexmo'),
	PARSE: 				require('parse/node'),
	PATH: 				require('path'),
	RANDOM: 			require('randomstring'),
	REDIS: 				require('redis'),
	SETTINGS: 			require('settings'),
	SEQUELIZE: 			require('sequelize'),
	UNDERSCORE: 		require('underscore'),
	WINSTON: 			require('winston')
};

var INIT = require(__dirname + '/system/init');

/* ------------------------------------------------------------------------
| 	CONFIGURATION
---------------------------------------------------------------------------
|
|	Load application configurations.
|
|	Author: @ais
|	Latest updates:
|	13 Oct 2015 - @ais - Created auto-import for configs
|
------------------------------------------------------------------------- */

var CONFIG_PATH 	= require(__dirname + '/application/config/filepath');
var CONFIG 			= INIT.CONFIG(MODULES, CONFIG_PATH);
CONFIG.FILE_PATH 	= CONFIG_PATH;

/* ------------------------------------------------------------------------
| 	GO! =9
---------------------------------------------------------------------------
|
|	Initialize application node.
|
|	Author: @ais
|
------------------------------------------------------------------------- */

require(__dirname + '/application/server')(MODULES, CONFIG);
