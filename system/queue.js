'use strict';

/* ------------------------------------------------------------------------
| 	QUEUE.JS
---------------------------------------------------------------------------
|
|	Configure & initialize queues.
|
|	Author: @ais
|	Latest updates:
|	29 Oct 2015 - @ais - Create queues.
|
------------------------------------------------------------------------- */

module.exports = function (args) {

	let MODULES = args.MODULES;
	let CONFIG	= args.CONFIG;

	let fs 		= MODULES.FS;
	let path	= MODULES.PATH;

	fs.readdirSync(CONFIG.FILE_PATH.queues).filter(function (file) {
		var queueName = file.replace(/\.js$/, '').replace(/\.coffee$/, '');
		require(path.join(CONFIG.FILE_PATH.queues, queueName))(args);
	});
};