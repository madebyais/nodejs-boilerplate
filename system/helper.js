'use strict';

/* ------------------------------------------------------------------------
| 	HELPER.JS
---------------------------------------------------------------------------
|
|	Configure & initialize helpers.
|
|	Author: @ais
|	Latest updates:
|	20 Oct 2015 - @ais - Create helpers.
|
------------------------------------------------------------------------- */

module.exports = function (args) {

	var MODULES = args.MODULES;
	var CONFIG	= args.CONFIG;

	var fs 		= MODULES.FS;
	var path	= MODULES.PATH;

	/******************************************************************
	|	Author: @ais
	|	Description: Auto-generate & auto-init helpers.
	******************************************************************/

	var helpers = {}

	fs.readdirSync(CONFIG.FILE_PATH.helpers).filter(function (file) {
		var helperName = file.replace(/\.js$/, '').replace(/\.coffee$/, '');
		helpers[helperName] = require(path.join(CONFIG.FILE_PATH.helpers, helperName));
	});

	return helpers;
};