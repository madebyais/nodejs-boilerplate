'use strict';

/* ------------------------------------------------------------------------
| 	SERVICE.JS
---------------------------------------------------------------------------
|
|	Configure & initialize services.
|
|	Author: @ais
|	Latest updates:
|	20 Oct 2015 - @ais - Create services.
|
------------------------------------------------------------------------- */

module.exports = function (args) {

	var MODULES = args.MODULES;
	var CONFIG	= args.CONFIG;

	var fs 		= MODULES.FS;
	var path	= MODULES.PATH;

	/******************************************************************
	|	Author: @ais
	|	Description: Auto-generate & auto-init services.
	******************************************************************/

	var services = {}

	fs.readdirSync(CONFIG.FILE_PATH.services).filter(function (file) {
		var serviceName = file.replace(/\.js$/, '').replace(/\.coffee$/, '');
		services[serviceName] = require(path.join(CONFIG.FILE_PATH.services, serviceName))(args);
	});

	return services;
};