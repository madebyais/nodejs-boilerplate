'use strict';

/* ------------------------------------------------------------------------
| 	MODEL.JS
---------------------------------------------------------------------------
|
|	Configure & initialize models/schemas.
|
|	Author: @ais
|	Latest updates:
|	20 Oct 2015 - @ais - Create models/schemas.
|
------------------------------------------------------------------------- */

module.exports = function (args) {

	var MODULES = args.MODULES;
	var CONFIG	= args.CONFIG;

	var fs 		= MODULES.FS;
	var log		= MODULES.LOG;
	var path	= MODULES.PATH;
	var initSequelize;

	/******************************************************************
	|	Author: @ais
	|	Description: Enabled several database engines.
	******************************************************************/

	var DB_ENGINE = {
		SEQUELIZE: MODULES.SEQUELIZE
	}

	/******************************************************************
	|	Author: @ais
	|	Description: Auto-generate ORMs.
	******************************************************************/

	var schemas = {};

	CONFIG.connections.databases.forEach(function(db){
		if (db) {
			if (db.type == 'postgres') {
				schemas[db.name] = new MODULES.SEQUELIZE(db.type + '://' + db.username + ':' + db.password + '@' + db.host + ':' + db.port + '/' + db.dbName);
				initSequelize = new MODULES.SEQUELIZE(db.type + '://' + db.username + ':' + db.password + '@' + db.host + ':' + db.port + '/' + db.dbName);
			} else if (db.type == 'mongodb') {
				// ADD MONGODB ORM IF NEEDED
			}
		}
	});

	/******************************************************************
	|	Author: @ais
	|	Description: Auto-generate & auto-init models/schemas.
	******************************************************************/

	var models = {}
	var listModels = []

	fs.readdirSync(CONFIG.FILE_PATH.models).filter(function (file) {
		var modelName = file.replace(/\.js$/, '').replace(/\.coffee$/, '');
		if(schemas['postgresql']) {
			models[modelName] = schemas['postgresql'].import(path.join(CONFIG.FILE_PATH.models, modelName));
			listModels.push(modelName);
		}
	});

	/******************************************************************
	|	Author: @ais
	|	Description: Auto-associating models.
	******************************************************************/

	if (CONFIG.orms.enableRelations) {
		CONFIG.orms.relations.forEach(function (relationConfig) {
			models[relationConfig.parentTable][relationConfig.type](models[relationConfig.childTable], relationConfig.opts);
		});
	}

	/******************************************************************
	|	Author: @ais
	|	Description: Auto-sync for all models. Let sequelize handles it
	******************************************************************/

	let syncForce = {};

	if (CONFIG.connections.syncForce) {
		syncForce = { force: true };
		log.warn('Set syncForce=true');
	}
	
	schemas.postgresql.sync(syncForce);

	args.MODULES.initSequelize = initSequelize;

	return models;
};
