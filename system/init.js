'use strict';

module.exports = {

	/**********************************************
	|	Author: @ais
	|	Description: Compile config files.
	**********************************************/
	
	CONFIG: function (MODULES, CONFIG_PATH) {
		var CONFIG = {};
		var excludePath = CONFIG_PATH._excludePath;
		for(var configType in CONFIG_PATH) { 
			if(configType && excludePath.indexOf(configType) < 0 && configType != '_excludePath') 
				CONFIG[configType] = new MODULES.SETTINGS(require(CONFIG_PATH[configType])); 
		}
		return CONFIG;
	},

	/**********************************************
	|	Author: @ais
	|	Description: Compile helper files.
	**********************************************/

	HELPERS: function (MODULES, CONFIG_PATH) {

	}

}