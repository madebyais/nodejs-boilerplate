module.exports = function (args) {

	var MODULES = args.MODULES;
	var CONFIG	= args.CONFIG;

	var fs 		= MODULES.FS;
	var path	= MODULES.PATH;

	var controllers = {}

	fs.readdirSync(CONFIG.FILE_PATH.controllers).filter(function (file) {
		var controllerName = file.replace(/\.js$/, '').replace(/\.coffee$/, '');
		controllers[controllerName] = require(path.join(CONFIG.FILE_PATH.controllers, controllerName))(args);
	});

	return controllers;

};