'use strict';

/* ------------------------------------------------------------------------
| 	ROUTER.JS
---------------------------------------------------------------------------
|
|	Initialize application endpoints based-on router config files.
|
|	Author: @ais
|	Latest updates:
|	14 Oct 2015 - @ais - Create a logic to auto-generate common router
|
------------------------------------------------------------------------- */

module.exports = function (args) {

	var MODULES		= args.MODULES;
	var CONFIG		= args.CONFIG;
	var app			= args.app;
	var controllers = args.controllers;
	var helpers		= args.helpers;

	var async 	= MODULES.ASYNC;
	var fs 		= MODULES.FS;
	var log 	= MODULES.LOG;
	var _		= MODULES.UNDERSCORE;

	/* ------------------------------------------------------------------------
	| 	APPLICATION ROUTER
	---------------------------------------------------------------------------
	|
	|	Configure & initialize application router.
	|
	|	Author: @ais
	|	Latest updates:
	|	14 Oct 2015 - @ais - Create application router logic.
	|
	------------------------------------------------------------------------- */

	var tempListFirstEndpoint = [];

	var routerMethods = {

		/******************************************************************
		|	Author: @ais
		|	Description: Auto-generate application endpoints.
		******************************************************************/

		generateEndpoint: function (ROUTER_CONFIG, parentEndpoint, firstEndpoint) {
			var self = this;
			var CRUDList = []

			for (var endpoint in ROUTER_CONFIG) {
				var urlPath, lastEndpoint;
				var method 			= endpoint.split(' ')[0].toLowerCase();
				var routerParams 	= ROUTER_CONFIG[endpoint];

				parentEndpoint 	= parentEndpoint.toLowerCase();
				firstEndpoint 	= (firstEndpoint) ? firstEndpoint.toLowerCase() : null;
				lastEndpoint  	= endpoint.split(' ')[1];

				if (parentEndpoint == 'index') {
					if (!firstEndpoint) urlPath = lastEndpoint;
				} else {
					if(firstEndpoint && firstEndpoint == 'index') {
						(lastEndpoint == '/') ? urlPath = '/' + parentEndpoint : urlPath = '/' + parentEndpoint + lastEndpoint;
					} else {
						(lastEndpoint == '/') ? urlPath = '/' + parentEndpoint + '/' + firstEndpoint : urlPath = '/' + parentEndpoint + '/' + firstEndpoint + lastEndpoint;
					}
				}

				var baseEndpoint = {
					parentEndpoint: parentEndpoint,
					firstEndpoint: firstEndpoint,
					lastEndpoint: lastEndpoint
				};

				self.endpointHandler(method, urlPath, routerParams);
				CRUDList.push(baseEndpoint);
				tempListFirstEndpoint.push(firstEndpoint);
			}

			if (tempListFirstEndpoint.indexOf(firstEndpoint.toLowerCase()) < 0)
				CRUDList.push({ parentEndpoint: parentEndpoint, firstEndpoint: firstEndpoint });

			CRUDList.forEach(function (baseEndpoint) {
				self.endpointCRUDHandler(baseEndpoint);
			});
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Get controller name & method.
		******************************************************************/

		getController: function (controllerStr) {
			try {
				var controllerFileName = controllerStr.split('.')[0];
				var controllerFuncName = controllerStr.split('.')[1];
				return controllers[controllerFileName][controllerFuncName];
			} catch (e) {
				console.dir(e);
			}
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Call controller for process.
		******************************************************************/

		execController: function (controllerStr, previousData, req, res, next) {
			var callController = this.getController(controllerStr);
			callController(previousData, req, res, function (err, data){
				if (err) { return next(err); }
				next(null, _.extend(previousData, data));
			});
		},

		/******************************************************************
		|	Author: @ais
		|	Description: Handle all requests to custom endpoints.
		******************************************************************/

		endpointHandler: function (method, urlPath, routerParams) {
			var self = this;

			if (routerParams && routerParams.controller && routerParams.controller.length > 0) {
				app[method](urlPath, function(req, res, next){

					var controllerMethods = [];
					routerParams.controller.forEach(function(controllerStr){
						controllerMethods.push(function (previousData, cb){
							if (!cb) { cb = previousData; previousData = {}; }
							self.execController(controllerStr, previousData, req, res, cb);
						});
					});

					async.waterfall(controllerMethods, function (err, data){
						if (err) return res.json({ status: 'error', data: err });
						res.json({ status: 'success', data: data });
					});

				});
			} else {
				log.warn("Can't find any controller for url path: " + urlPath);
			}

		},

		/******************************************************************
		|	Author: @ais
		|	Description: Handle all requests for CRUD endpoints.
		******************************************************************/

		endpointCRUDHandler: function (baseEndpoint) {
			var self 		= this;
			var urlPath 	= '/' + baseEndpoint.parentEndpoint + '/' + baseEndpoint.firstEndpoint;
			var modelName 	= helpers.Model.getModelName(baseEndpoint.firstEndpoint);

			/******************************************************************
			|	Author: @ais
			|	Description: CRUD - get all records.
			******************************************************************/

			app.get(urlPath, function (req, res, next) {
				self.execController('CRUDController.findAll', { model: modelName }, req, res, next)
			});

			/******************************************************************
			|	Author: @ais
			|	Description: CRUD - create a new record.
			******************************************************************/

			app.post(urlPath, function (req, res, next) {
				self.execController('CRUDController.create', { model: modelName }, req, res, next)
			});

			/******************************************************************
			|	Author: @ais
			|	Description: CRUD - all method using `:id`.
			******************************************************************/

			urlPath += '/:id';

			/******************************************************************
			|	Author: @ais
			|	Description: CRUD - get a record based on `:id`.
			******************************************************************/

			app.get(urlPath, function (req, res, next) {
				self.execController('CRUDController.findById', { model: modelName }, req, res, next)
			});

			/******************************************************************
			|	Author: @ais
			|	Description: CRUD - update a records based on `:id`.
			******************************************************************/

			app.put(urlPath, function (req, res, next) {
				self.execController('CRUDController.update', { model: modelName }, req, res, next)
			});

			/******************************************************************
			|	Author: @ais
			|	Description: CRUD - delete a records based on `:id`.
			******************************************************************/

			app.delete(urlPath, function (req, res, next) {
				self.execController('CRUDController.destroy', { model: modelName }, req, res, next)
			});
		}
	};

	console.time('InitializeRouters');

	/******************************************************************
	|	Author: @ais
	|	Description: Read all router configurations.
	******************************************************************/

	fs.readdirSync(CONFIG.FILE_PATH.routers).filter(function (baseFile) {
		var tempPath = CONFIG.FILE_PATH.routers + baseFile;
		var ROUTER_CONFIG;

		/******************************************************************
		|	Author: @ais
		|	Description: Check whether it is a file or a directory.
		|	If a directory then add the directory name into an endpoint,
		|	else if a file, directly add as an endpoint.
		******************************************************************/

		if (fs.statSync(tempPath).isDirectory()) {
			fs.readdirSync(tempPath).filter(function (file) {
				ROUTER_CONFIG = require(tempPath + '/' + file);
				routerMethods.generateEndpoint(ROUTER_CONFIG, baseFile, file.replace('.coffee', ''));
			});
		} else {
			ROUTER_CONFIG = require(tempPath);
			routerMethods.generateEndpoint(ROUTER_CONFIG, baseFile.replace('.coffee', ''));
		}
	});
	console.timeEnd('InitializeRouters');
};